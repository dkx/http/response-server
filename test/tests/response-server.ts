import {expect} from 'chai';
import fetch from 'node-fetch';
import {ResponseServer} from '../../lib';


let server: ResponseServer;


describe('#proxyMiddleware', () => {

	beforeEach((done) => {
		server = new ResponseServer;
		server.run(8000, done);
	});

	afterEach((done) => {
		server.close(done);
	});

	it('should respond', async () => {
		server.respondWith({
			statusCode: 404,
			statusMessage: 'Not found',
			body: 'Data was not found',
		});

		const response = await fetch('http://localhost:8000');

		expect(response.status).to.be.equal(404);
		expect(response.statusText).to.be.equal('Not found');
		expect(await response.text()).to.be.equal('Data was not found');
	});

});
