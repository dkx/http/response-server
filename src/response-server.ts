import {Server} from '@dkx/http-server';
import {responseMiddleware, ResponseOptions} from '@dkx/http-middleware-response';


export class ResponseServer
{


	private server: Server = new Server;


	public respondWith(options: ResponseOptions): void
	{
		this.server.use(responseMiddleware(options));
	}


	public run(port: number, fn?: () => void): void
	{
		this.server.run(port, fn);
	}


	public close(fn?: () => void): void
	{
		this.server.close(fn);
	}

}
