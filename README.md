# DKX/Http/ResponseServer

Testing response server based on [@dkx/http-server](https://gitlab.com/dkx/http/server).

## Installation

```bash
$ npm install --save @dkx/http-response-server
```

or with yaml

```bash
$ yarn add @dkx/http-response-server
```

## Usage

```javascript
const {ResponseServer} = require('@dkx/http-response-server');

const server = new ResponseServer;

server.respondWith({
	statusCode: 404,
	statusMessage: 'Not found',
	body: 'Data was not found',
});

server.run(8080, function() {
	console.log('Server is listening on port 8080');
});

server.close(function() {
	console.log('Server was closed');
});
```

See [@dkx/http-middleware-response](https://gitlab.com/dkx/http/middlewares/response) for all available `respondWith()` 
options.
